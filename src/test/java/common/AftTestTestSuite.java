package common;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import apicheck.ConvertTransactions;

@RunWith(Suite.class)
@Suite.SuiteClasses({

	ConvertTransactions.class

})
public class AftTestTestSuite {
}
