package steps;

import java.text.DecimalFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.LinkedHashMap;
import java.util.Map;


import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import io.restassured.response.Response;
import net.serenitybdd.rest.SerenityRest;
import net.thucydides.core.annotations.Step;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import static org.hamcrest.Matchers.is;

public class ConvertTransactionsSteps {
    private String TRANSACTION_SERVICE = "https://7np770qqk5.execute-api.eu-west-1.amazonaws.com/prod/";
    private String EXCHANGE_URL = "https://api.exchangeratesapi.io/";
    private Response response;
    private String createdAt;
    private String currency;
    private Integer amount;
    private Float rate;
    private Float convertedAmount;
    private String checksum;

    @Step
    public void getOriginalTransaction(){
        response = SerenityRest.when().get(TRANSACTION_SERVICE + "get-transaction");
        createdAt = response.getBody ().path ("createdAt"  );
        currency = response.getBody ().path ("currency"  );
        amount = response.getBody ().path ("amount"  );
        checksum = response.getBody ().path ("checksum"  );
    }

    @Step
    public void transactionExecutedSuccessfully(){
        response.then().statusCode(200);
    }

    @Step
    public void getExchangeRate() {

        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDate localDate = LocalDate.now();

        response = SerenityRest.when().get(EXCHANGE_URL + dtf.format ( localDate ) + "?base=EUR");
        rate = response.getBody().path("rates." + currency);
    }

    @Step
    public void calculateFinalAmount(){

        DecimalFormat df = new DecimalFormat ("#.####");

        if (amount>0)
            convertedAmount = Float.parseFloat(df.format (  amount/rate));
        else
            convertedAmount = Float.parseFloat(df.format (amount));
    }

    @Step
    public void postConvertedTransaction(){

        //JSONObject inArrayData = new JSONObject();
        Map<String, Object> inArrayData = new LinkedHashMap<> ();
        inArrayData.put("createdAt", createdAt);
        inArrayData.put("currency", currency);
        inArrayData.put("amount", amount);
        inArrayData.put("convertedAmount", convertedAmount);
        inArrayData.put("checksum",  checksum);

        JSONArray inArray = new JSONArray();
        inArray.add ( inArrayData );

        JSONObject data = new JSONObject();
        data.put("transaction", inArray);

        response = SerenityRest.given().accept("application/json").contentType("application/json").body ( data ).when()
                .post (TRANSACTION_SERVICE + "process-transactions");

    }


    @Step
    public void transactionPostedSuccessfully(){

        response.then().body("success", is(true));
        response.then().body("passed", is(1));
        response.then().body("failed", is(0));
    }

    private class Json {
    }
}