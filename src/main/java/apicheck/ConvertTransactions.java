package apicheck;

import net.serenitybdd.junit.runners.SerenityRunner;
import net.thucydides.core.annotations.Steps;

import org.junit.Test;
import org.junit.runner.RunWith;
import steps.ConvertTransactionsSteps;

@RunWith(SerenityRunner.class)
public class ConvertTransactions {
    @Steps
    ConvertTransactionsSteps convertSteps;

    @Test
    public void verifyThatWeCanPostTransaction() {
        for (int i = 0; i < 100; i++) {
            convertSteps.getOriginalTransaction ();
            convertSteps.transactionExecutedSuccessfully ();
            convertSteps.getExchangeRate ();
            convertSteps.transactionExecutedSuccessfully ();
            convertSteps.calculateFinalAmount ();
            convertSteps.postConvertedTransaction ();
            convertSteps.transactionExecutedSuccessfully ();
            convertSteps.transactionPostedSuccessfully ();

        }
    }

}